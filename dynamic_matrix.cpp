#include <iostream>

int main()
{
    int n, m, i, j, min;
    int ind_c = 0;
    std::cin >> n >> m;

    if (n < 100)
        if (m < 100)
        {
            int** a = new int* [n];
            for (i = 0; i < n; i++)
                a[i] = new int[m];
            for (i = 0; i < n; i++)
                for (j = 0; j < m; j++)
                {
                    std::cin >> a[i][j];
                }
            min = a[0][0];
            for (i = 0; i < n; i++)
                for (j = 0; j < m; j++)
                    if (min > a[i][j])
                    {
                        min = a[i][j];
                        ind_c = j;
                        for (i = 0; i < n; i++)
                            if (a[i][ind_c] < 0)
                                a[i][ind_c] = 0;
                    }
            std::cout << ind_c << std::endl;
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m; j++)
                    std::cout << a[i][j] << " ";
                std::cout << std::endl;
            }
            for (int i = 0; i < n; i++)
                delete[] a[i];
            delete[] a;
        }
    
    return 0;
}