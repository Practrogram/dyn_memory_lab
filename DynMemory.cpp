﻿#include <iostream>
#include <algorithm>
const int M = 1000;

int main()
{
    int n;
    std::cin >> n;
    int* a = new int[M];
    for (int i = 0; i < n; i++)
        std::cin >> a[i];
    int* f = new int[n];
    int* max = new int[n];

    for (int i = 0; i < n - 1; i++)
        for (int j = i + 1; j < n; j++)
        {
            int p = a[i];
            f[i] = 0;
            while (p > 9)
            {
                p /= 10;
                f[i] = p % 10;
            }
            max[i] = 0;
            while (p > 9)
            {
                if (p % 10 > max[i])
                    max[i] = p % 10;
                p /= 10;
            }
            if (f[i] > f[j])
                std::swap(f[i], f[j]);
            else if (max[i] > max[j])
                    std::swap(max[i], max[j]);
            else if (a[i] > a[j])
                    std::swap(a[i], a[j]);
        }

    for (int i = 0; i < n; i++)
        std::cout << a[i] << " ";
    delete[] a;
    delete[] f;
    delete[] max;

    return 0;
}